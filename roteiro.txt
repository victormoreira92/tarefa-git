git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init: cria um repositório vazio do Git, ou reinicializar um repositório.
Ao ser criada a pasta Git são criadas as seguintes pastas: refs/heads, refs/tags, e arquivo de template, também uma branch inicial sem commits é criada.

git status: mostra a arvóre de arquivos de trabalho, essencialmente o comando mostra as diferenças entre o arquivo de índice e o commit HEAD atual.

git add arquivo/diretório : Este comando atualiza o índice usando o conteúdo atual encontrado na árvore de trabalho,
 para preparar o conteúdo preparado para o próximo commit.

git add --all = git add . : Adiciona todos os arquivos e pastas que estão na árvore de arquivos, e prepara para o próximo commit. 

git commit -m “Primeiro commit” : Crie um novo commit contendo o conteúdo atual do índice e a mensagem de log fornecida descrevendo as alterações. 
O novo commit é um filho direto de HEAD.

-------------------------------------------------------

git log : Mostra as mensagens de commit, dos logs mais antigos paara os recentes.
git log arquivo: Mostra todas as mensagens de commits de um arquivo.
git reflog : Mostra resumidamente os commits de um repositório.

-------------------------------------------------------

git show :Mostra a  mensagem de log e o diff textual de todos os commits. Ele também apresenta o merge commit em um formato especial.
git show <commit> : Mostra as diferenças de um commit.

-------------------------------------------------------

git diff : Mostra as diferenças entre o arquivo da árvore de trabalho e 
o commit realizado.
git diff <commit1> <commit2> : Mostra o que foi alterado entre os dois commits de uma branch.

-------------------------------------------------------

git branch : Mostra a branch local onde está ocorrendo os commits e a arvore de trabalho.
git branch -r : Mostra a branch remota onde está ocorrendo a modificação.
git branch -a: Mostra a branch local e também a remota.
git branch -d <branch_name> : Deleta a branch local
git branch -D <branch_name> : Delelta a branch remota
git branch -m <nome_novo> : Dá um novo nome para a branch onde está ocorrendo a produção. 
git branch -m <nome_antigo> <nome_novo>: Modifica o nome antigo para um novo nome.

-------------------------------------------------------

git checkout <branch_name> : Troca de branch da atual para uma outra branch.
git checkout -b <branch_name> : Cria uma nova branch e troca da atual para a nova branch. 

-------------------------------------------------------

git merge <branch_name> : Une as modificações salvas de uma branch 
para a principal, usualmente a main.

-------------------------------------------------------

git clone : Clona um repositório em um diretório recém-criado, cria
 ramificações de rastreamento remoto para cada ramificação no repositório clonado e 
 cria e faz check-out de uma ramificação inicial que é bifurcada a partir da ramificação 
 atualmente ativa do repositório clonado.

git pull: Incorpora as muundanças de uma pasta remota para a local
git push: Envia os logs do repositório local para o remoto.

-------------------------------------------------------

git remote: mostra a branch da pasta remota
git remote -v: mostra a URL da pasta remota de destino.
git remote add origin <url> :Especifica o diretório remoto de destino 
onde será colocada as modificações
git remote rename <old-name> <new-name>: Especifica um novo nome para a pasta remota de 
de destino

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLxjCnO-f9JPT6Kuww_d2TJQK31Mz-cwR4
